package com.rachellima.empresas;
import com.rachellima.empresas.components.DaggerApplicationComponent;
import com.rohitss.uceh.UCEHandler;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;


public class EnterpriseApplication extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerApplicationComponent.builder().create(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize UCE_Handler Library
        new UCEHandler.Builder(this).build();
    }
}
