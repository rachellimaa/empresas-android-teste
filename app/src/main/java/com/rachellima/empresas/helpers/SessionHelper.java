package com.rachellima.empresas.helpers;

import com.rachellima.empresas.models.Session;
import com.rachellima.empresas.services.IToken;
import com.rachellima.empresas.services.ITokenHolder;

import javax.inject.Inject;

public class SessionHelper implements ITokenHolder {
    private Session mSession;
    private IToken mToken;

    @Inject
    SessionHelper() {
    }

    @Override
    public IToken getToken() {
        return mToken;
    }

    @Override
    public void setToken(IToken token) {
        mToken = token;
    }

    public void clear() {
        mToken = null;
        mSession = null;
    }

    public Session getSession() {
        return mSession;
    }

    public void setSession(Session session) {
        this.mSession = session;
    }
}
