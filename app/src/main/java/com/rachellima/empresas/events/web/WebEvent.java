package com.rachellima.empresas.events.web;

import com.rachellima.empresas.events.BaseEvent;

public abstract class WebEvent<T> extends BaseEvent<T> {

    WebEvent() {
    }

    WebEvent(T data) {
        super(data);
    }

    WebEvent(Throwable error) {
        super(error);
    }
}
