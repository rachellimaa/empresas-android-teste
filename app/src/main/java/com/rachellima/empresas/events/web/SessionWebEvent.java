package com.rachellima.empresas.events.web;

import com.rachellima.empresas.models.Session;

public class SessionWebEvent extends WebEvent<Session> {

    public SessionWebEvent() {
    }

    public SessionWebEvent(Session data) {
        super(data);
    }

    public SessionWebEvent(Throwable mError) {
        super(mError);
    }
}
