package com.rachellima.empresas.events;

import androidx.annotation.NonNull;

import java.lang.reflect.Array;

public abstract class BaseEvent<T> {

    protected Throwable mError;
    protected T mData;

    protected BaseEvent() {
    }

    protected BaseEvent(T data) {
        this.mData = data;
    }

    protected BaseEvent(Throwable error) {
        this.mError = error;
    }

    public Throwable getError() {
        return mError;
    }

    public T getData() {
        return mData;
    }

    public void setError(Throwable error) {
        this.mError = error;
    }

    public void setData(T data) {
        this.mData = data;
    }

    public boolean isSuccess() {
        return mError == null;
    }

    @NonNull
    @Override
    public String toString() {
        if (mError == null) {
            if (mData instanceof Object[]) {
                return this.getClass().getSimpleName() + " { Size: " + Array.getLength(mData) + " }";
            } else {
                return this.getClass().getSimpleName() + " { Data: " + mData + " }";
            }
        } else {
            return this.getClass().getSimpleName() + " { Error: " + mError + " }";
        }
    }
}

