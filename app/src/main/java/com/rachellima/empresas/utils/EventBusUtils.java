package com.rachellima.empresas.utils;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;

public class EventBusUtils {
    private static final String TAG = EventBusUtils.class.getSimpleName();

    private static EventBus mEventBus = EventBus.getDefault();

    private EventBusUtils() {
    }

    public static void post(Object object) {
        mEventBus.post(object);
    }

    public static void register(Object object) {
        if (!mEventBus.isRegistered(object)) {
            Log.i(TAG, "Subscriber registered: " + object);
            mEventBus.register(object);
        }
    }

    public static void unregister(Object object) {
        if (mEventBus.isRegistered(object)) {
            Log.i(TAG, "Subscriber unregistered: " + object);
            mEventBus.unregister(object);
        }
    }
}
