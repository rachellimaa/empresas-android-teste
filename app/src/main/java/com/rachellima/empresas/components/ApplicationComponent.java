package com.rachellima.empresas.components;

import com.rachellima.empresas.EnterpriseApplication;
import com.rachellima.empresas.modules.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;

@Singleton
@Component(modules = ApplicationModule.class)
interface ApplicationComponent extends AndroidInjector<EnterpriseApplication> {
    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<EnterpriseApplication> {
    }
}

