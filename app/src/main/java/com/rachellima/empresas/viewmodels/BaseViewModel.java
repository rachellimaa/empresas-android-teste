package com.rachellima.empresas.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.rachellima.empresas.utils.EventBusUtils;

/**
 * Base class of ViewModels
 */
public abstract class BaseViewModel<T> extends ViewModel {
    protected MutableLiveData<T> mData;

    public BaseViewModel() {
        mData = new MutableLiveData<>();

        EventBusUtils.register(this);
    }

    public MutableLiveData<T> data() {
        if (mData == null) {
            mData = new MutableLiveData<>();
        }
        return mData;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        EventBusUtils.unregister(this);
        mData = null;
    }

    /**
     * Method to clean all unnecessary references
     */
    public void unload() {
        onCleared();
    }
}
