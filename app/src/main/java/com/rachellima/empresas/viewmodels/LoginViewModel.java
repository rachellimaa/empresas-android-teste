package com.rachellima.empresas.viewmodels;

import androidx.annotation.NonNull;

import com.rachellima.empresas.events.web.SessionWebEvent;
import com.rachellima.empresas.services.IEnterpriseRestAPIClient;

public class LoginViewModel extends BaseViewModel<SessionWebEvent> {
    private final IEnterpriseRestAPIClient mEnterpriseWebClient;

    public LoginViewModel(IEnterpriseRestAPIClient enterpriseWebClient) {
        mEnterpriseWebClient = enterpriseWebClient;
    }

    /**
     * Login by email
     */
    public void doLogin(final @NonNull String email, @NonNull String password) {
        mEnterpriseWebClient.login(email, password);
    }

}
