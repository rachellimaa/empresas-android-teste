package com.rachellima.empresas.services;

import android.content.Context;

import androidx.annotation.NonNull;

import com.rachellima.empresas.events.SessionRequest;
import com.rachellima.empresas.events.web.SessionWebEvent;
import com.rachellima.empresas.models.Session;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnterpriseRestAPIClient extends BaseRespApiEnterprise implements IEnterpriseRestAPIClient{
    private IEnterpriseRestAPI mIEnterpriseRestAPI;
    private Context mContext;

    public EnterpriseRestAPIClient(IEnterpriseRestAPI mIEnterpriseRestAPI, Context mContext) {
        this.mIEnterpriseRestAPI = mIEnterpriseRestAPI;
        this.mContext = mContext;
    }

    @Override
    public void login(@NonNull String email, @NonNull String password) {
        Call<Session> call = mIEnterpriseRestAPI.loginByEmail(new SessionRequest(email, password));

        call.enqueue(new Callback<Session>() {
            @Override
            public void onResponse(@NonNull Call<Session> call, @NonNull Response<Session> response) {
                Session session = response.body();
                if (response.isSuccessful()){
                    postResponseEvent(new SessionWebEvent(session));
                }else{
                    handleWebError(new SessionWebEvent(), null);
                }
            }

            @Override
            public void onFailure(Call<Session> call, Throwable t) {
                handleWebError(new SessionWebEvent(), t);
            }
        });
    }
}
