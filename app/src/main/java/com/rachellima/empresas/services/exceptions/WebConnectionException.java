package com.rachellima.empresas.services.exceptions;

import android.os.Build;

import androidx.annotation.RequiresApi;

public class WebConnectionException extends Exception {
    public WebConnectionException() {
    }

    public WebConnectionException(String message) {
        super(message);
    }

    public WebConnectionException(String message, Throwable cause) {
        super(message, cause);
    }

    public WebConnectionException(Throwable cause) {
        super(cause);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public WebConnectionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
