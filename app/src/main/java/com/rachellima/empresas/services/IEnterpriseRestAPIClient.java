package com.rachellima.empresas.services;

import androidx.annotation.NonNull;

public interface IEnterpriseRestAPIClient {

    void login(@NonNull String email, @NonNull String password);
}
