package com.rachellima.empresas.services;

public interface ITokenHolder {
        IToken getToken();

        void setToken(IToken token);

    }
