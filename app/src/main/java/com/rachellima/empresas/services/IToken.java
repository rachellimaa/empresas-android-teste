package com.rachellima.empresas.services;

public interface IToken<T> {
    T getValue();
}