package com.rachellima.empresas.services;

import com.rachellima.empresas.events.SessionRequest;
import com.rachellima.empresas.models.Session;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IEnterpriseRestAPI {

    @POST("users/auth/sign_in")
    Call<Session> loginByEmail(@Body SessionRequest request);
}
