package com.rachellima.empresas.services;

import android.content.Context;
import android.util.Log;

import com.rachellima.empresas.R;
import com.rachellima.empresas.events.BaseEvent;
import com.rachellima.empresas.events.web.WebEvent;
import com.rachellima.empresas.services.exceptions.WebConnectionException;
import com.rachellima.empresas.utils.EventBusUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseRespApiEnterprise {
    protected final String TAG = this.getClass().getSimpleName();

    protected Context mContext;
    protected List<Call<?>> mCalls = new ArrayList();

    public <T extends BaseEvent> void postResponseEvent(T response) {
        Log.d(TAG, "Response => " + response.toString());

        EventBusUtils.post(response);
    }

    public <T extends WebEvent> void handleWebError(T response, Throwable throwable) {
        response.setError(new WebConnectionException(
                mContext.getString(R.string.connection_error), throwable));

        postResponseEvent(response);
    }


    protected abstract class ReqCallback<T> implements Callback<T> {
        public ReqCallback(Call<?> call) {
            mCalls.add(call);
        }

        @Override
        public void onResponse(Call<T> call, Response<T> response) {
            mCalls.remove(call);

            if (!call.isCanceled()) {
                doResponse(call, response);
            }
        }

        @Override
        public void onFailure(Call<T> call, Throwable t) {
            mCalls.remove(call);

            if (!call.isCanceled()) {
                doFailure(call, t);
            }
        }

        protected abstract void doResponse(Call<T> call, Response<T> response);

        protected abstract void doFailure(Call<T> call, Throwable t);
    }
}
