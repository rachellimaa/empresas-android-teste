package com.rachellima.empresas.modules;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rachellima.empresas.BuildConfig;
import com.rachellima.empresas.helpers.SessionHelper;
import com.rachellima.empresas.services.IEnterpriseRestAPI;
import com.rachellima.empresas.services.IEnterpriseRestAPIClient;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class EnterpriseWebModule {
    private static final int CONNECTION_TIMEOUT = 60;
    private static final int CONN_READ_TIMEOUT = CONNECTION_TIMEOUT;
    private static final int CONN_WRITE_TIMEOUT = CONNECTION_TIMEOUT;
    private static final String DEV_HOST = "https://empresas.ioasys.com.br";
    private static final String API_VERSION = "v1";
    private static final String BASE_URL = DEV_HOST+"/api/"+API_VERSION+"/";

    @Provides
    IEnterpriseRestAPI provideEnterpriseRestApi(Gson gson, SessionHelper sessionHelper) {
        return provideRetrofit(gson, sessionHelper).create(IEnterpriseRestAPI.class);
    }

    private Retrofit provideRetrofit(Gson gson, SessionHelper sessionHelper) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(provideOkHttpClient(sessionHelper))
                .build();
    }

    private OkHttpClient provideOkHttpClient(SessionHelper sessionHelper) {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();
        //Interceptor for debug only
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.level(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(CONN_READ_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .writeTimeout(CONN_WRITE_TIMEOUT, TimeUnit.SECONDS)
                .interceptors().add(provideInterceptor(sessionHelper));
        return httpClientBuilder.build();
    }

    private Interceptor provideInterceptor(final SessionHelper sessionHelper) {
        return new Interceptor() {
            @NotNull
            @Override
            public Response intercept(@NotNull Chain chain) throws IOException {
                Request newRequest = null;
                if (sessionHelper.getToken() != null) {
                    newRequest = chain.request().newBuilder()
                            .addHeader("Content-Type", "application/json")
                            .addHeader("access-token", "access-token " + sessionHelper.getToken().getValue())
                            .addHeader("client", "client" + sessionHelper.getToken().getValue())
                            .addHeader("uid", "uid " + sessionHelper.getSession().getEmail())
                            .build();
                }
                assert newRequest != null;
                return chain.proceed(newRequest);
            }
        };
    }

}
