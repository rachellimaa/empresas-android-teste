package com.rachellima.empresas.modules;

import android.content.Context;

import com.rachellima.empresas.EnterpriseApplication;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.android.AndroidInjectionModule;

@Module(includes = {
        AndroidInjectionModule.class,
        EnterpriseWebModule.class,
        ViewModelModule.class,
})
public abstract class ApplicationModule {
    @Binds
    abstract Context provideApplicationContext(EnterpriseApplication application);
}
