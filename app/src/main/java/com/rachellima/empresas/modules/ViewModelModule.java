package com.rachellima.empresas.modules;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.rachellima.empresas.services.IEnterpriseRestAPIClient;
import com.rachellima.empresas.viewmodels.LoginViewModel;
import com.rachellima.empresas.viewmodels.ViewModelFactory;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Map;

import javax.inject.Provider;

import dagger.MapKey;
import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;

@Module
public class ViewModelModule {
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @MapKey
    @interface ViewModelKey {
        Class<? extends ViewModel> value();
    }

    @Provides
    ViewModelProvider.Factory providesViewModelFactory(Map<Class<? extends ViewModel>,
            Provider<ViewModel>> providerMap) {
        return new ViewModelFactory(providerMap);
    }

    @Provides
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    ViewModel providesLoginViewModel(IEnterpriseRestAPIClient enterpriseWebClient) {
        return new LoginViewModel(enterpriseWebClient);
    }
}