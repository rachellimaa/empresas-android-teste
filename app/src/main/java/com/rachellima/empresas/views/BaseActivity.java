package com.rachellima.empresas.views;

import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;

import com.rachellima.empresas.utils.EventBusUtils;

import dagger.android.AndroidInjection;

public class BaseActivity extends AppCompatActivity {
    protected final String TAG = this.getClass().getSimpleName();
    protected Lifecycle.Event mLifecycleEvent = Lifecycle.Event.ON_ANY;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        tryInjectDependencies();
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onResume() {
        super.onResume();
        mLifecycleEvent = Lifecycle.Event.ON_RESUME;

        tryRegisterEventBus();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mLifecycleEvent = Lifecycle.Event.ON_PAUSE;

    }

    private void tryInjectDependencies() {
        try {
            AndroidInjection.inject(this);
        } catch (Exception e) { // this activity is not a dependency injector
            Log.i(TAG, "tryInjectDependencies: " + e.getMessage());
        }
    }

    private void tryRegisterEventBus() {
        try {
            EventBusUtils.register(this);
        } catch (Exception e) { // this activity is not a dependency injector
            Log.i(TAG, "tryRegisterEventBus: " + e.getMessage());
        }
    }
}
