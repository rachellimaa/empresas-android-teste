package com.rachellima.empresas.views;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.rachellima.empresas.EnterpriseApplication;
import com.rachellima.empresas.R;
import com.rachellima.empresas.events.web.SessionWebEvent;
import com.rachellima.empresas.models.Session;
import com.rachellima.empresas.viewmodels.LoginViewModel;
import com.rachellima.empresas.views.HomeActivity;

import javax.inject.Inject;

public class LoginActivity extends BaseActivity {
    private Button mLoginButton;
    private EditText mEmail;
    private EditText mPassword;

    private LoginViewModel mLoginViewModel;

    @Inject
    ViewModelProvider.Factory mViewModelFactory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        setupViewModel();
        goToHome();
    }

    private void setupViewModel() {
        // get an instance of the ViewModel
        mLoginViewModel = ViewModelProviders.of(this,
                mViewModelFactory).get(LoginViewModel.class);

        // set the method that will observe changes after ClienteWebEvent is triggered
        mLoginViewModel.data().observe(this,
                this::handleSessionWebLoginResult);
    }

    private void goToHome() {
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), HomeActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initView() {
        mLoginButton = findViewById(R.id.search_button);
        mEmail = findViewById(R.id.text_email);
        mPassword = findViewById(R.id.text_password);
    }

    @Override
    protected void onResume() {
        super.onResume();
        onRequestContentData();
    }

    private void onRequestContentData() {
        doLogin();
    }

    private void doLogin() {
        mLoginViewModel.doLogin(getEmail(), getPassword());
    }

    private String getEmail() {
        if (mEmail != null) {
            return mEmail.getText().toString();
        }
        return null;
    }

    private String getPassword() {
        if (mPassword != null) {
            return mPassword.getText().toString();
        }
        return null;
    }

    private void handleSessionWebLoginResult(SessionWebEvent event) {
        if (event.isSuccess()) {
            Session session = event.getData();
            Log.d("XX", "req: " + session.getEmail());
        } else {
            Log.e("XX", "erro: " + event.getError());
        }
    }

}
